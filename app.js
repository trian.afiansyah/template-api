const express = require('express')
const morgan = require('morgan')
const bParser = require('body-parser')
const http = require('http')
const dotenv = require('dotenv')

// Init ENV
dotenv.config()

// Init Express
let app = express()
app.disable('x-powered-by')
app.use(bParser.json({limit:'10mb'}))
app.use(bParser.urlencoded({extended: true}))

// Routers
const router = require('./routes/router')
app.use('/api/v1',router)


// Init Server
let port = process.env.PORT || 3333
let httpServer = http.createServer(app)
httpServer.listen(port, () => {
    console.log(` Listening to ${port}`)
})