# Template Api

Template For making api using node js 

## Installation


```bash
npm i
```

## ENV Template

```
NODE_ENV = 
PORT =
DB_TYPE = 
DB_HOST = 
DB_USERNAME = 
DB_PASSWORD = 
DB_SELECT = 

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)