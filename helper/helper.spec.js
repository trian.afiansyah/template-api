const faker = require('faker')

const helper = require('./helper')
const config = require('../config/keys')

describe('Helper Test', () => {
    it('Should Encrypt & Decrypt Data', () => {
        let random = faker.lorem.word()
        let encrypted = helper.secEncryptV3(config.key, config.iv, random)
        let decrypted = helper.secDecryptV3(config.key, config.iv, encrypted)
        expect(encrypted).toMatch(/[a-zA-Z0-9]/gmi)
        expect(decrypted).toBe(random)
    })
})