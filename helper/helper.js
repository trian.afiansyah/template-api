const crypto = require('crypto')

exports.checkNullQueryAll = (object) => {
    if (object == undefined || object == '' || (Array.isArray(object)&& object.length == 0) ) {
        return true;
    };
    return false;
}

exports.isEmailT = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

exports.secEncryptV3 = (key, iv,data) => {
    let encoded = JSON.stringify(data);
    encoded = new Buffer.from(encoded).toString('base64');
    let cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let crypted = cipher.update(encoded, 'utf-8', 'hex');
    crypted += cipher.final('hex');

    return crypted;
};

exports.secDecryptV3 = (key, iv, data) => {
    let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    let decrypted = decipher.update(data, 'hex', 'utf-8');
    try {
        decrypted += decipher.final('utf-8');
        decrypted = new Buffer.from(decrypted, 'base64').toString('ascii')
        decrypted = JSON.parse(decrypted);
    } catch (error) {
        decrypted = null;
    }

    return decrypted;
};

exports.templateResponse = (status, success, msg, data, error) => {
    return {
        status,
        success,
        msg,
        data,
        error
    }
}


exports.responseCode = {
    OK: 200,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOTFOUND: 404,
    BADREQUEST: 400,
    METHODENOTALLOWED: 405,
    CONFLICT:409,
    INTERNALSERVERERROR:500
}

exports.responseSuccess = {
    FALSE:false,
    TRUE: true
}

exports.universalMessages = {
    INVALID_FORMAT_PARAMETER:'invalid_parameter',
    EMPTY: 'empty',
    OK: 'ok'
}