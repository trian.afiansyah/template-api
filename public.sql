/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 110006
 Source Host           : localhost:5432
 Source Catalog        : sharing_session
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110006
 File Encoding         : 65001

 Date: 10/12/2020 18:22:05
*/


-- ----------------------------
-- Sequence structure for MST_USERS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."MST_USERS_seq";
CREATE SEQUENCE "public"."MST_USERS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for MST_USERS
-- ----------------------------
DROP TABLE IF EXISTS "public"."MST_USERS";
CREATE TABLE "public"."MST_USERS" (
  "ID" int4 NOT NULL DEFAULT nextval('"MST_USERS_seq"'::regclass),
  "NAME" varchar(255) COLLATE "pg_catalog"."default",
  "EMAIL" varchar(255) COLLATE "pg_catalog"."default",
  "PASSWORD" varchar(255) COLLATE "pg_catalog"."default",
  "PHONE" varchar(255) COLLATE "pg_catalog"."default",
  "ADDRESS" varchar(255) COLLATE "pg_catalog"."default",
  "D_CREATED_DATE" timestamp(6),
  "D_UPDATED_DATE" timestamp(6)
)
;

-- ----------------------------
-- Records of MST_USERS
-- ----------------------------
INSERT INTO "public"."MST_USERS" VALUES (1, 'Brett Greenholt', 'Maudie.Considine62@gmail.com', 'd008028307130983cd560234a3da6368c85bf808147899afb8daea3d5528b656', '(907) 813-1268 x34294', '993 Willms Corners', '2020-12-09 17:10:06.643', '2020-12-09 17:10:06.643');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."MST_USERS_seq"', 2, true);

-- ----------------------------
-- Primary Key structure for table MST_USERS
-- ----------------------------
ALTER TABLE "public"."MST_USERS" ADD CONSTRAINT "MST_USERS_pkey" PRIMARY KEY ("ID");
